/* RESPUESTAS */
/* 1. Nuestro archivo compilado no se altero, pero como es ahora estatica se almacena en la parte de la memoria RAM llamada Data. Adicional, si no se hubiera inicializado pero sigue siendo estatico se llenarian automaticamente el arreglo con ceros */
/* 2. El offset dado un arreglo de un tipo de dato determinado representa la distancia que existe entre los punteros de dos elementos contiguos */
/* 3. Generalmente las direcciones de memoria se representan mediante sistemas de numeracion hexadecimal debido a que son numeros relativamente grandes, debido a que nuestro interes es visualizar los offsets debemos de convertirlos a enteros y en este caso son enteros largos debido a que se usa sistema de numeracion hexadecimal porque son numeros muy grandes */
#include <stdio.h>
#define MAX 10

static char caracteres[MAX] = {'a','b','c'};
static long enteros_largos[MAX];
static char *punteros[MAX] = {"hola","mundo","hello","world"};

void iterar_enteros(int *);
void iterar_caracteres(char *);
void iterar_enteros_largos(long *);
void iterar_punteros(char **);

int main()
{
	static int enteros[MAX];
	enteros[0] = 100;
	enteros[1] = 300;
	enteros[5] = 50;

	int x;
	for(int i=0; i<MAX; i++){
		x = enteros[i];
		printf("%d",x);
		printf("\n");
	}
	iterar_enteros(enteros);
	
	printf("\n----------------------------\n");
	
	iterar_caracteres(caracteres);
	
	printf("\n----------------------------\n");
	
	enteros_largos[1] = 3251;
	enteros_largos[3] = 25413;
	enteros_largos[6] = 241253;	
	
	iterar_enteros_largos(enteros_largos);
	
	printf("\n----------------------------\n");

	iterar_punteros(punteros);
}

void iterar_enteros(int *enteros)
{
	/* Declaracion de variables tipo puntero a entero*/
	int *p_anterior = NULL;
	int *p;
	/* Iterara diez veces por indice, empezando en cero */
	for(int i=0;i<MAX;i++)
	{
		/* ARITMETICA DE PUNTEROS */
		/* A p que es un puntero a entero se le asignara la variable "enteros" */
		/* que tambien es puntero a entero sumado el indice i, es decir que nuestra p */
		/* ira cambiando a medida que se itere sobre el arreglo de enteros */
		/* Por ejemplo, teniendo el arreglo asi: [100, 300, x, x, 50, x, x, x, x, x] */
		/* Siendo inicializado como un arreglo de enteros de diez elementos, pero solo */
		/* se han agregado 3 de 10 posibles elementos a agregarse al arreglo, entonces la "x" */
		/* representara los lugares que aun no han sido ocupados por el usuario o programador y */
		/* de acuerdo con c, estos espacios sin usar pueden tener valores basura, cualquier cosa que */
		/* no tiene nada que ver con el programa que construimos, entonces al leerlo e imprimir los valores */
		/* elemento a elemento, de seguro tendremos nuestros tres valores insertados, pero el resto seran valores basura */
		p = enteros + i;
		printf("\n");
		printf("El valor de p es: %d\n", *p);
		printf("La direcci�n de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

void iterar_caracteres(char *caracteres)
{
  char *c_anterior;
  char *c;
  
	for(int i=0;i<MAX;i++)
	{
		c = caracteres + i;

		printf("El valor de c es: %c\n", *c);
		printf("La direcci�n de c es: %p\n", c);
		printf("El offset entre direcciones es: %ld\n\n",(long)c - (long)c_anterior);  
    
		c_anterior = c;
	}  
}

void iterar_enteros_largos(long *enteros_largos)
{
  long *f_anterior;
  long *f;
  
	for(int i=0;i<MAX;i++)
	{
		f = enteros_largos + i;

		printf("El valor de f es: %ld\n", *f);
		printf("La direcci�n de f es: %p\n", f);
		printf("El offset entre direcciones es: %ld\n\n",(long)f - (long)f_anterior);  
    
		f_anterior = f;

	}
}
void iterar_punteros(char **punteros){
	char **p_anterior = NULL;
	char **p;
	for(int i=0; i<MAX; i++){
		p = punteros + i;
		printf("El valor de p es: %s\n", *p);
		printf("La direccion de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n\n", (long)p - (long)p_anterior);

		p_anterior = p;
	}
}
